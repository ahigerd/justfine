#ifndef JF_BROWSER_H
#define JF_BROWSER_H

#include <QWidget>
#include "jfapi.h"
class QStandardItemModel;
class QListView;
class QComboBox;

class Browser : public QWidget {
Q_OBJECT
public:
  Browser(QWidget* parent = nullptr);

signals:
  void requestMainMenu();
  void requestMenu(const QString& key);

public slots:
  void pushMenu(const QString& key, const QList<JfMenuItem>& items);

private slots:
  void backToMenu(int index);
  void itemSelected(const QModelIndex& index);

protected:
  void showEvent(QShowEvent*);

private:
  QComboBox* stack;
  QListView* menu;
  QStandardItemModel* model;
  QString pendingMenuName, pendingMenuKey, currentMenuKey;
};

#endif
