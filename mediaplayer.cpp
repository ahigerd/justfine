#include "mediaplayer.h"
#ifndef QT_FEATURE_gstreamer_app
#define QT_FEATURE_gstreamer_app -1
#endif
#ifdef JF_GSTREAMER
#include "3rdparty/qgstreamerplayersession_p.h"
#include "3rdparty/qgstreamerplayercontrol_p.h"
#endif
#include <QFile>
#include <QDir>
#include <QMediaService>

struct MediaPlayerPrivate {
  MediaPlayerPrivate(MediaPlayer* pub) : p(pub), audioSink(nullptr) {}

  MediaPlayer* p;
  GstElement* audioSink;
  QByteArray type;
};

MediaPlayer* MediaPlayer::create(QObject* parent) {
  QByteArray type = qgetenv("QT_GSTREAMER_PLAYBIN_AUDIOSINK");
  return MediaPlayer::create(type, parent);
}

MediaPlayer* MediaPlayer::create(const QByteArray& type, QObject* parent) {
#ifdef JF_GSTREAMER
  bool wasSet = qEnvironmentVariableIsSet("QT_GSTREAMER_PLAYBIN_AUDIOSINK");
  QByteArray previousType = qgetenv("QT_GSTREAMER_PLAYBIN_AUDIOSINK");
  MediaPlayer* player = nullptr;
  if (type != previousType) {
    qputenv("QT_GSTREAMER_PLAYBIN_AUDIOSINK", type);
    player = new MediaPlayer(parent);
    if (!wasSet) {
      qunsetenv("QT_GSTREAMER_PLAYBIN_AUDIOSINK");
    } else {
      qputenv("QT_GSTREAMER_PLAYBIN_AUDIOSINK", previousType);
    }
  } else
#endif
  {
    player = new MediaPlayer(parent);
  }
  return player;
}

MediaPlayer::MediaPlayer(QObject* parent)
: QMediaPlayer(parent, QMediaPlayer::StreamPlayback), d(new MediaPlayerPrivate(this)) {
#ifdef JF_GSTREAMER
  if (service()->metaObject()->className() == QByteArray("QGstreamerPlayerService")) {
    QMediaService* service = this->service();
    QGstreamerPlayerControl* control = static_cast<QGstreamerPlayerControl*>(service->requestControl(QMediaPlayerControl_iid));
    QGstreamerPlayerSession* session = control->session();
    d->audioSink = gst_bin_get_by_name(GST_BIN(session->m_audioSink), "audiosink");
    d->type = QByteArray(G_OBJECT_TYPE_NAME(d->audioSink)).replace("Gst", "").toLower();
  } else
#endif
  {
    d->audioSink = nullptr;
    d->type = QByteArray();
  }
}

MediaPlayer::~MediaPlayer() {
  delete d;
}

bool MediaPlayer::hasDeviceControl() const {
#ifdef JF_GSTREAMER
  return d->type == "alsasink";
#else
  return false;
#endif
}

QMap<QString, QString> MediaPlayer::availableDevices() const {
  QMap<QString, QString> result;
  result[" Default"] = QString();
#ifdef JF_GSTREAMER
  if (d->type != "alsasink") {
    return result;
  }
  QFile cardsFile("/proc/asound/cards");
  if (!cardsFile.open(QIODevice::ReadOnly)) {
    return result;
  }
  QStringList lines = QString::fromUtf8(cardsFile.readAll()).split('\n');
  for (int i = 0; i < lines.count() - 1; i += 2) {
    QString index = lines[i].section('[', 0, 0);
    QString id = lines[i].section(']', 0, 0).mid(index.length() + 1).trimmed();
    QString name = lines[i + 1].section(" at 0x", 0, 0).trimmed();
    index = index.trimmed();
    QString basePath = QString("/proc/asound/card%1/").arg(index);
    for (const QString& filename : QDir(basePath).entryList(QStringList() << "pcm*p")) {
      QFile pcmInfo(QString("%1/%2/info").arg(basePath).arg(filename));
      if (!pcmInfo.exists() || !pcmInfo.open(QIODevice::ReadOnly)) {
        continue;
      }
      QStringList info = QString::fromUtf8(pcmInfo.readAll()).split('\n');
      for (const QString& line : info) {
        if (line.startsWith("name:")) {
          QString pcmName = line.section(": ", 1).trimmed();
          QString device = QString("alsa|default:CARD=%1,DEV=%2").arg(id).arg(filename.mid(3).chopped(1));
          QString deviceName = QString("%1: %2").arg(name, pcmName);
          result[deviceName] = device;
        }
      }
    }
  }
#endif
  return result;
}

QString MediaPlayer::currentDevice() const {
#ifdef JF_GSTREAMER
  if (d->type == "alsasink") {
    char* value = nullptr;
    //g_object_set(audiosink, "device", "redirect", NULL);
    g_object_get(d->audioSink, "device", &value, NULL);
    return "alsa|" + QString::fromUtf8(value);
  }
#endif
  return QString();
}

void MediaPlayer::setCurrentDevice(const QString& dev) {
  if (!hasDeviceControl()) {
    return;
  }
#ifdef JF_GSTREAMER
  if (d->type == "alsasink" && d->audioSink) {
    g_object_set(d->audioSink, "device", qPrintable(dev.section('|', 1)), NULL);
  }
#endif
}
