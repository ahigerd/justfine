#ifndef JF_JFAPI_H
#define JF_JFAPI_H

#include <QNetworkAccessManager>
#include <QJsonObject>
#include <QPair>
class QWebSocket;

struct JfMenuItem {
  QString sortName;
  QString displayName;
  QByteArray id;
  QByteArray imageTag;
  bool isFolder;
};

class JfApi : public QObject {
Q_OBJECT
public:
  static JfApi* instance();
  static QString generateDeviceId();

  JfApi(QObject* parent = nullptr);
  ~JfApi();

  QString server() const;
  void setServer(const QString& address);

  QString deviceId() const;
  void setDeviceId(const QString& id);

  QString userId() const;
  void login(const QString& username, const QString& password);

  void openSocket();

public slots:
  void requestMainMenu();
  void requestMenu(const QString& key);

signals:
  void loggedIn();
  void loginFailed();
  void loggedOut();
  void receivedMenu(const QString& key, const QList<JfMenuItem>& items);
  void _emitReply(QNetworkReply*, const QJsonObject&, const QVariant&);

private slots:
  void handleReply();
  void loginResponse(QNetworkReply* reply, const QJsonObject& body);
  void viewResponse(QNetworkReply* reply, const QJsonObject& body, const QVariant& userData);

private:
  QNetworkReply* post(const QString& endpoint, const QJsonObject& payload, const char* slot, const QVariant& userData = QVariant());
  QNetworkReply* post(const QString& endpoint, const QJsonObject& payload, const QMap<QString,QString>& headers, const char* slot, const QVariant& userData = QVariant());
  QNetworkReply* get(const QString& endpoint, const char* slot, const QVariant& userData = QVariant());
  QNetworkReply* get(const QString& endpoint, const QMap<QString,QString>& headers, const char* slot, const QVariant& userData = QVariant());
  QNetworkReply* request(const QString& endpoint, const QByteArray& payload, const QMap<QString,QString>& headers, const char* slot, const QVariant& userData = QVariant());

  QNetworkAccessManager* net;
  QString _server, _deviceId, _userId, _accessToken;
  QMap<QNetworkReply*, QPair<QVariant, const char*>> _replySlots;
  QWebSocket* wss;
};
#define JF_API JfApi::instance()

#endif
