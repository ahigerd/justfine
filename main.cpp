#include <QApplication>
#include <QComboBox>
#include <QFileDialog>
#include <QPushButton>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include "jfapi.h"
#include "mediaplayer.h"
#include "prefspanel.h"
#include "browser.h"

int main(int argc, char** argv) {
  QApplication app(argc, argv);

  JfApi jf;
  jf.setServer("https://xmpps.greenmaw.com/jellyfin/");

#if 0
  MediaPlayer* p1 = MediaPlayer::create();
  qDebug() << p1->availableDevices();
  MediaPlayer* p2 = MediaPlayer::create("alsasink");
  qDebug() << p2->availableDevices();
  /*
  GstIterator *it = gst_bin_iterate_elements (GST_BIN (session->m_audioSink));
  GValue elem = G_VALUE_INIT;
  while (gst_iterator_next(it, &elem) == GST_ITERATOR_OK)
  {
    g_print("%s\n",
        gst_element_get_name(g_value_get_object(&elem)));
    g_value_reset(&elem);
  }

  g_value_unset(&elem);
  gst_iterator_free(it);
  */

  QString filename;
  QWidget window;
  QGridLayout* layout = new QGridLayout(&window);

  layout->addWidget(new QLabel("Device:"), 0, 0);
  QComboBox* fDevice = new QComboBox;
  auto devices = p2->availableDevices();
  for (const QString& deviceName : devices.keys()) {
    fDevice->addItem(deviceName, devices[deviceName]);
  }
  //fDevice->addItems(audioSelector->availableOutputs());
  layout->addWidget(fDevice, 0, 1);

  layout->addWidget(new QLabel("File:"), 1, 0);
  QPushButton* bBrowse = new QPushButton("Browse...");
  layout->addWidget(bBrowse, 1, 1);

  QPushButton* bPlay1 = new QPushButton("Play 1");
  layout->addWidget(bPlay1, 2, 0, 1, 2);
  QPushButton* bPlay2 = new QPushButton("Play 2");
  layout->addWidget(bPlay2, 3, 0, 1, 2);

  QObject::connect(bBrowse, &QPushButton::clicked, [&]() {
      filename = QFileDialog::getOpenFileName(&window);
      bBrowse->setText(filename.isEmpty() ? "Browse..." : filename);
  });

  QObject::connect(bPlay1, &QPushButton::clicked, [&]() {
      p1->setMedia(QMediaContent(QUrl("file://" + filename)));
      p1->play();
  });
  QObject::connect(bPlay2, &QPushButton::clicked, [&]() {
      p2->setCurrentDevice(fDevice->currentData().toString());
      p2->setMedia(QMediaContent(QUrl("file://" + filename)));
      p2->play();
  });

  layout->addWidget(new QLabel("Username:"), 4, 0);
  QLineEdit* fUsername = new QLineEdit;
  layout->addWidget(fUsername, 4, 1);

  layout->addWidget(new QLabel("Password:"), 5, 0);
  QLineEdit* fPassword = new QLineEdit;
  fPassword->setEchoMode(QLineEdit::Password);
  layout->addWidget(fPassword, 5, 1);

  QPushButton* bLogin = new QPushButton("Log In");
  layout->addWidget(bLogin, 6, 0, 1, 2);
  QObject::connect(bLogin, &QPushButton::clicked, [&]() {
    jf.login(fUsername->text(), fPassword->text());
  });

  window.show();
#endif

  if (jf.userId().isEmpty()) {
    PrefsPanel* pp = new PrefsPanel;
    pp->show();
  } else {
    //jf.requestMainMenu();
    Browser* b = new Browser;
    b->show();
  }

  return app.exec();
}
