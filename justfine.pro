TEMPLATE = app
TARGET = justfine
INCLUDEPATH += .
QT = core network gui widgets multimedia multimediawidgets websockets
unix:!macx {
  CONFIG += link_pkgconfig
  PKGCONFIG += gstreamer-1.0
  DEFINES += QT_FEATURE_gstreamer_app=-1
}

HEADERS += mediaplayer.h   jfapi.h   prefspanel.h   browser.h
SOURCES += mediaplayer.cpp jfapi.cpp prefspanel.cpp browser.cpp main.cpp
