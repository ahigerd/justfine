#include "browser.h"
#include <QVBoxLayout>
#include <QStandardItemModel>
#include <QListView>
#include <QComboBox>

Browser::Browser(QWidget* parent) : QWidget(parent) {
  QVBoxLayout* layout = new QVBoxLayout(this);

  stack = new QComboBox(this);
  stack->addItem("Main Menu", "_mainMenu");
  layout->addWidget(stack, 0);

  menu = new QListView(this);
  model = new QStandardItemModel(this);
  menu->setModel(model);
  menu->setEditTriggers(QListView::NoEditTriggers);
  layout->addWidget(menu, 1);

  QObject::connect(this, SIGNAL(requestMainMenu()), JF_API, SLOT(requestMainMenu()));
  QObject::connect(this, SIGNAL(requestMenu(QString)), JF_API, SLOT(requestMenu(QString)));
  QObject::connect(menu, SIGNAL(activated(QModelIndex)), this, SLOT(itemSelected(QModelIndex)));
  QObject::connect(stack, SIGNAL(currentIndexChanged(int)), this, SLOT(backToMenu(int)));
  QObject::connect(JF_API, SIGNAL(receivedMenu(QString, QList<JfMenuItem>)), this, SLOT(pushMenu(QString, QList<JfMenuItem>)));
}

void Browser::pushMenu(const QString& key, const QList<JfMenuItem>& items) {
  qDebug() << "pushMenu" << key << pendingMenuKey;
  if (key != pendingMenuKey) {
    // Bogus result, probably the user clicking around too fast
    return;
  }
  int pos = stack->findData(key);
  if (pos < 0) {
    int current = stack->findData(currentMenuKey);
    while (stack->count() > current + 1) {
      stack->removeItem(current + 1);
    }
    stack->addItem(pendingMenuName, key);
    stack->setCurrentIndex(stack->count() - 1);
  } else {
    stack->setCurrentIndex(pos);
  }
  pendingMenuName = QString();
  pendingMenuKey = QString();
  currentMenuKey = key;

  model->clear();
  for (const auto& item : items) {
    QStandardItem* i = new QStandardItem(item.displayName);
    i->setData(item.id, Qt::UserRole);
    i->setData(item.sortName, Qt::UserRole + 1);
    model->appendRow(i);
  }
}

void Browser::showEvent(QShowEvent*) {
  pendingMenuKey = "_mainMenu";
  emit requestMainMenu();
}

void Browser::backToMenu(int index) {
  QString key = stack->itemData(index).toString();
  qDebug() << "backToMenu" << index << key;
  if (key == currentMenuKey || key == pendingMenuKey) {
    // Don't need to do anything
    return;
  }
  pendingMenuKey = key;
  if (key == "_mainMenu") {
    emit requestMainMenu();
  } else {
    emit requestMenu(key);
  }
}

void Browser::itemSelected(const QModelIndex& index) {
  QString key = model->data(index, Qt::UserRole).toString();
  if (key == currentMenuKey || key == pendingMenuKey) {
    // user is clicking around too fast
    return;
  }
  pendingMenuName = model->data(index, Qt::DisplayRole).toString();
  pendingMenuKey = key;
  emit requestMenu(key);
}
