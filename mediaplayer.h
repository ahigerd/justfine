#ifndef JF_MEDIAPLAYER_H
#define JF_MEDIAPLAYER_H

#include <QMediaPlayer>
#include <QMap>
#if defined(Q_OS_UNIX) && !defined(Q_OS_MACOS)
#define JF_GSTREAMER
#endif

class MediaPlayerPrivate;
class MediaPlayer : public QMediaPlayer {
Q_OBJECT
public:
  ~MediaPlayer();
  static MediaPlayer* create(QObject* parent = nullptr);
  static MediaPlayer* create(const QByteArray& type, QObject* parent = nullptr);

  bool hasDeviceControl() const;
  QMap<QString, QString> availableDevices() const;
  QString currentDevice() const;
  void setCurrentDevice(const QString& dev);

private:
  friend class MediaPlayerPrivate;
  MediaPlayer(QObject* parent = nullptr);
  MediaPlayerPrivate* d;
};

#endif
