#include "jfapi.h"
#include <QJsonDocument>
#include <QJsonArray>
#include <QNetworkReply>
#include <QNetworkCookieJar>
#include <QNetworkCookie>
#include <QWebSocket>
#include <QSettings>

static const QString
  JF_AUTHENTICATE("emby/Users/authenticatebyname"),
  JF_FULL_CAPS("Sessions/Capabilities/Full"),
  JF_MAINMENU("users/{USER}/views"),
  JF_SUBMENU("users/{USER}/items?sortby=isfolder,sortname&parentid=%1"),
  JF_SOCKET("socket?api_key=%1&deviceId=%2");

static JfApi* JfApi_instance = nullptr;

JfApi* JfApi::instance() {
  return JfApi_instance;
}

QString JfApi::generateDeviceId() {
  QString deviceId;
  for (int i = 0; i < 10; i++) {
    deviceId += char(qrand() % 10 + '0');
  }
  return deviceId;
}

JfApi::JfApi(QObject* parent)
: QObject(parent), net(new QNetworkAccessManager(this)), wss(nullptr) {
  JfApi_instance = this;
  QSettings settings;
  if (settings.contains("deviceId")) {
    _deviceId = settings.value("deviceId").toString();
  } else {
    _deviceId = generateDeviceId();
    settings.setValue("deviceId", _deviceId);
  }
  _server = settings.value("server").toString();
  _userId = settings.value("userId").toString();
  _accessToken = _userId.isEmpty() ? QString() : settings.value("accessToken").toString();
  if (_accessToken.isEmpty()) _userId = QString();
  qDebug() << "on startup" << _userId << _accessToken;
  settings.beginGroup("cookies");
  QNetworkCookieJar* jar = net->cookieJar();
  for (const QString& key : settings.allKeys()) {
    for (const QNetworkCookie& cookie : QNetworkCookie::parseCookies(settings.value(key).toByteArray())) {
      jar->insertCookie(cookie);
    }
  }
}

JfApi::~JfApi() {
  JfApi_instance = nullptr;
  delete net;
}

QString JfApi::server() const {
  return _server;
}

void JfApi::setServer(const QString& address) {
  if (address.endsWith("/")) {
    _server = address;
  } else {
    _server = address + "/";
  }
  QSettings settings;
  settings.setValue("server", _server);
}

QString JfApi::deviceId() const {
  return _deviceId;
}

void JfApi::setDeviceId(const QString& id) {
  QSettings settings;
  if (id.isEmpty()) {
    _deviceId = generateDeviceId();
  } else {
    _deviceId = id;
  }
  settings.setValue("deviceId", _deviceId);
}

QNetworkReply* JfApi::post(const QString& endpoint, const QJsonObject& payload, const char* slot, const QVariant& userData) {
  return post(endpoint, payload, QMap<QString,QString>(), slot, userData);
}

QNetworkReply* JfApi::post(const QString& endpoint, const QJsonObject& payload, const QMap<QString,QString>& header, const char* slot, const QVariant& userData) {
  return request(endpoint, QJsonDocument(payload).toJson(QJsonDocument::Compact), header, slot, userData);
}

QNetworkReply* JfApi::get(const QString& endpoint, const char* slot, const QVariant& userData) {
  return request(endpoint, QByteArray(), QMap<QString,QString>(), slot, userData);
}

QNetworkReply* JfApi::get(const QString& endpoint, const QMap<QString,QString>& header, const char* slot, const QVariant& userData) {
  return request(endpoint, QByteArray(), header, slot, userData);
}

QNetworkReply* JfApi::request(const QString& endpoint, const QByteArray& payload, const QMap<QString,QString>& header, const char* slot, const QVariant& userData) {
  QUrl url(_server + QString(endpoint).replace("{USER}", _userId));
  QNetworkRequest req(url);
  bool hasContentType = false, hasAccept = false, hasToken = false;
  for (const QString& key : header.keys()) {
    if (key.toLower() == "content-type") {
      hasContentType = true;
    } else if (key.toLower() == "accept") {
      hasAccept = true;
    } else if (key.toLower() == "x-emby-token") {
      hasToken = true;
    }
    req.setRawHeader(qPrintable(key), qPrintable(header[key]));
  }
  if (!hasAccept) {
    req.setRawHeader("accept", "application/json; charset=utf-8");
  }
  if (!hasToken && !_accessToken.isEmpty()) {
    req.setRawHeader("x-emby-token", qPrintable(_accessToken));
    // TODO: allow naming the device in a preferences UI
    req.setRawHeader("x-emby-authorization", qPrintable(
          "mediabrowser client=\"justfine\", "
          "device=\"Linux-PC\", deviceid=\"" + _deviceId + "\", "
          "version=\"0.0.1\", token=\"" + _accessToken + "\""));
  } else {
    req.setRawHeader("x-emby-authorization", qPrintable("mediabrowser client=\"justfine\", device=\"Linux-PC\", deviceid=\"" + _deviceId + "\", version=\"0.0.1\""));
  }
  QNetworkReply* reply;
  if (!payload.isEmpty()) {
    if (!hasContentType) {
      req.setRawHeader("content-type", "application/json; charset=utf-8");
    }
    qDebug() << "POST" << url << header;
    reply = net->post(req, payload);
  } else {
    qDebug() << "GET" << url << header;
    reply = net->get(req);
  }
  _replySlots[reply] = QPair<QVariant, const char*>(userData, slot);
  QObject::connect(reply, SIGNAL(sslErrors(QList<QSslError>)), reply, SLOT(ignoreSslErrors()));
  QObject::connect(reply, SIGNAL(finished()), this, SLOT(handleReply()));
  return reply;
}

void JfApi::handleReply() {
  QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
  if (!reply) {
    qWarning("JfApi::handleReply: spurious slot activation");
    return;
  }

  QSettings settings;
  settings.beginGroup("cookies");
  settings.remove("");
  for (const QNetworkCookie& cookie : net->cookieJar()->cookiesForUrl(_server)) {
    qDebug() << "cookie" << cookie.toRawForm();
    settings.setValue(cookie.name(), cookie.toRawForm());
  }
  settings.endGroup();

  QByteArray content = reply->readAll();
  QJsonObject body = QJsonDocument::fromJson(content).object();
  if (body.isEmpty()) {
    qDebug() << content;
    if (content.contains("token")) {
      _accessToken = QString();
      settings.remove("accessToken");
      emit loggedOut();
    }
    return;
  } else if (body.contains("AccessToken")) {
    _accessToken = body.value("AccessToken").toString();
    settings.setValue("accessToken", _accessToken);
  }

  QPair<QVariant, const char*> slot = _replySlots.take(reply);
  qDebug() << "response:" << content;
  if (!slot.second) {
    // Fire-and-forget request
    reply->deleteLater();
    return;
  }

  auto conn = QObject::connect(this, SIGNAL(_emitReply(QNetworkReply*, QJsonObject, QVariant)), this, slot.second);
  _emitReply(reply, body, slot.first);
  QObject::disconnect(conn);
  // The slot is expected to clean up the reply object
}

QString JfApi::userId() const {
  return _userId;
}

void JfApi::login(const QString& username, const QString& password) {
  QJsonObject json;
  json["Username"] = username;
  json["Pw"] = password;
  //_accessToken = QString(); // clear the token to get a fresh login
  post(JF_AUTHENTICATE, json, SLOT(loginResponse(QNetworkReply*, QJsonObject)));
}

void JfApi::loginResponse(QNetworkReply* reply, const QJsonObject& body) {
  reply->deleteLater();
  if (reply->error()) {
    emit loginFailed();
  } else {
    QSettings settings;
    QJsonObject user = body.value("User").toObject();
    QString username = user.value("Name").toString();
    _userId = user.value("Id").toString();
    settings.setValue("userId", _userId);
    settings.setValue("username", username);
    emit loggedIn();
    requestMainMenu();
  }
}

void JfApi::requestMainMenu() {
  get(JF_MAINMENU, SLOT(viewResponse(QNetworkReply*, QJsonObject, QVariant)), "_mainMenu");

  if (wss) return;

  static const char* devProfile =
"    {\"MaxStreamingBitrate\":120000000,\"MaxStaticBitrate\":100000000,\"MusicStreamingTranscodingBitrate\":192000,"
"      \"DirectPlayProfiles\":["
"      {\"Container\":\"mp4,m4v\",\"Type\":\"Video\",\"VideoCodec\":\"h264,vp8,vp9\",\"AudioCodec\":\"mp3,aac,opus,flac,vorbis\"},"
"      {\"Container\":\"mkv\",\"Type\":\"Video\",\"VideoCodec\":\"h264,vp8,vp9\",\"AudioCodec\":\"mp3,aac,opus,flac,vorbis\"},"
"      {\"Container\":\"mov\",\"Type\":\"Video\",\"VideoCodec\":\"h264\",\"AudioCodec\":\"mp3,aac,opus,flac,vorbis\"},"
"      {\"Container\":\"opus\",\"Type\":\"Audio\"},"
"      {\"Container\":\"mp3\",\"Type\":\"Audio\",\"AudioCodec\":\"mp3\"},"
"      {\"Container\":\"aac\",\"Type\":\"Audio\"},"
"      {\"Container\":\"m4a,m4b\",\"AudioCodec\":\"aac\",\"Type\":\"Audio\"},"
"      {\"Container\":\"flac\",\"Type\":\"Audio\"},{\"Container\":\"webma,webm\",\"Type\":\"Audio\"},{\"Container\":\"wav\",\"Type\":\"Audio\"},{\"Container\":\"ogg\",\"Type\":\"Audio\"},"
"      {\"Container\":\"webm\",\"Type\":\"Video\",\"AudioCodec\":\"vorbis,opus\",\"VideoCodec\":\"VP8\"},{\"Container\":\"webm\",\"Type\":\"Video\",\"AudioCodec\":\"vorbis,opus\",\"VideoCodec\":\"VP9\"}"
"      ],\"TranscodingProfiles\":[{\"Container\":\"ts\",\"Type\":\"Audio\",\"AudioCodec\":\"aac\",\"Context\":\"Streaming\",\"Protocol\":\"hls\",\"MaxAudioChannels\":\"2\",\"MinSegments\":\"1\",\"BreakOnNonKeyFrames\":true},"
"     {\"Container\":\"aac\",\"Type\":\"Audio\",\"AudioCodec\":\"aac\",\"Context\":\"Streaming\",\"Protocol\":\"http\",\"MaxAudioChannels\":\"2\"},"
"{\"Container\":\"mp3\",\"Type\":\"Audio\",\"AudioCodec\":\"mp3\",\"Context\":\"Streaming\",\"Protocol\":\"http\",\"MaxAudioChannels\":\"2\"},"
"{\"Container\":\"opus\",\"Type\":\"Audio\",\"AudioCodec\":\"opus\",\"Context\":\"Streaming\",\"Protocol\":\"http\",\"MaxAudioChannels\":\"2\"},"
"{\"Container\":\"wav\",\"Type\":\"Audio\",\"AudioCodec\":\"wav\",\"Context\":\"Streaming\",\"Protocol\":\"http\",\"MaxAudioChannels\":\"2\"},"
"{\"Container\":\"opus\",\"Type\":\"Audio\",\"AudioCodec\":\"opus\",\"Context\":\"Static\",\"Protocol\":\"http\",\"MaxAudioChannels\":\"2\"},"
"{\"Container\":\"mp3\",\"Type\":\"Audio\",\"AudioCodec\":\"mp3\",\"Context\":\"Static\",\"Protocol\":\"http\",\"MaxAudioChannels\":\"2\"},"
"{\"Container\":\"aac\",\"Type\":\"Audio\",\"AudioCodec\":\"aac\",\"Context\":\"Static\",\"Protocol\":\"http\",\"MaxAudioChannels\":\"2\"},"
"{\"Container\":\"wav\",\"Type\":\"Audio\",\"AudioCodec\":\"wav\",\"Context\":\"Static\",\"Protocol\":\"http\",\"MaxAudioChannels\":\"2\"},"
"{\"Container\":\"mkv\",\"Type\":\"Video\",\"AudioCodec\":\"mp3,aac,opus,flac,vorbis\",\"VideoCodec\":\"h264,vp8,vp9\",\"Context\":\"Streaming\",\"MaxAudioChannels\":\"2\",\"CopyTimestamps\":true},"
"{\"Container\":\"mkv\",\"Type\":\"Video\",\"AudioCodec\":\"mp3,aac,opus,flac,vorbis\",\"VideoCodec\":\"h264,vp8,vp9\",\"Context\":\"Static\",\"MaxAudioChannels\":\"2\",\"CopyTimestamps\":true},"
"{\"Container\":\"ts\",\"Type\":\"Video\",\"AudioCodec\":\"mp3,aac,opus\",\"VideoCodec\":\"h264\",\"Context\":\"Streaming\",\"Protocol\":\"hls\",\"MaxAudioChannels\":\"2\",\"MinSegments\":\"1\",\"BreakOnNonKeyFrames\":true},"
"{\"Container\":\"webm\",\"Type\":\"Video\",\"AudioCodec\":\"vorbis\",\"VideoCodec\":\"vpx\",\"Context\":\"Streaming\",\"Protocol\":\"http\",\"MaxAudioChannels\":\"2\"},"
"{\"Container\":\"mp4\",\"Type\":\"Video\",\"AudioCodec\":\"mp3,aac,opus,flac,vorbis\",\"VideoCodec\":\"h264\",\"Context\":\"Static\",\"Protocol\":\"http\"}],"
"      \"ContainerProfiles\":[],"
"      \"CodecProfiles\":[{\"Type\":\"VideoAudio\",\"Codec\":\"aac\",\"Conditions\":[{\"Condition\":\"Equals\",\"Property\":\"IsSecondaryAudio\",\"Value\":\"false\",\"IsRequired\":false}]},{\"Type\":\"VideoAudio\",\"Conditions\":[{\"Condition\":\"Equals\",\"Property\":\"IsSecondaryAudio\",\"Value\":\"false\",\"IsRequired\":false}]},{\"Type\":\"Video\",\"Codec\":\"h264\",\"Conditions\":[{\"Condition\":\"NotEquals\",\"Property\":\"IsAnamorphic\",\"Value\":\"true\",\"IsRequired\":false},{\"Condition\":\"EqualsAny\",\"Property\":\"VideoProfile\",\"Value\":\"high|main|baseline|constrained baseline|high 10\",\"IsRequired\":false},{\"Condition\":\"LessThanEqual\",\"Property\":\"VideoLevel\",\"Value\":\"51\",\"IsRequired\":false}]}],"
"      \"SubtitleProfiles\":[{\"Format\":\"vtt\",\"Method\":\"External\"}],\"ResponseProfiles\":[{\"Type\":\"Video\",\"Container\":\"m4v\",\"MimeType\":\"video/mp4\"}],"
"      \"MaxStaticMusicBitrate\":320000}";

    QJsonObject caps;
    QJsonArray types{"Audio", "Video"};
    caps["PlayableMediaTypes"] = types;
    caps["SupportsMediaControl"] = true;
    caps["SupportsRemoteControl"] = true;
    caps["SupportsSync"] = true;
    caps["SupportsPersistentIdentifier"] = true;
    QJsonArray cmds{"MoveUp","MoveDown","MoveLeft","MoveRight","PageUp","PageDown",
      "PreviousLetter","NextLetter","ToggleOsd","ToggleContextMenu","Select","Back",
      "SendKey","SendString","GoHome","GoToSettings","VolumeUp","VolumeDown","Mute",
      "Unmute","ToggleMute","SetVolume","SetAudioStreamIndex","SetSubtitleStreamIndex",
      "DisplayContent","GoToSearch","DisplayMessage","SetRepeatMode","ChannelUp",
      "ChannelDown","PlayMediaSource","PlayTrailers"};
    caps["SupportedCommands"] = cmds;
    QJsonObject profile = QJsonDocument::fromJson(devProfile).object();
    //profile["MaxStreamingBitrate"] = 120000000;
    //profile["MaxStaticBitrate"] = 100000000;
    //profile["MaxStaticMusicBitrate"] = 320000;
    caps["DeviceProfile"] = profile;
    qDebug() << "sending caps" << caps;
    post(JF_FULL_CAPS, caps, nullptr);

    openSocket();
}

void JfApi::requestMenu(const QString& key) {
  get(JF_SUBMENU.arg(key), SLOT(viewResponse(QNetworkReply*, QJsonObject, QVariant)), key);
}

void JfApi::viewResponse(QNetworkReply* reply, const QJsonObject& body, const QVariant& userData) {
  QList<JfMenuItem> items;
  for (const QJsonValue& jsonValue : body.value("Items").toArray()) {
    JfMenuItem item;
    QJsonObject json = jsonValue.toObject();
    item.sortName = json.value("SortName").toString();
    item.displayName = json.value("Name").toString();
    item.id = json.value("Id").toString().toUtf8();
    item.imageTag = json.value("ImageTags").toObject().value("Primary").toString().toUtf8();
    item.isFolder = json.value("IsFolder").toBool();
    if (item.sortName.isEmpty()) {
      item.sortName = item.displayName.toLower();
    }
    //qDebug() << item.sortName << item.displayName << item.id << item.imageTag;
    items << item;
  }
  emit receivedMenu(userData.toString(), items);
  reply->deleteLater();
}

void JfApi::openSocket() {
  wss = new QWebSocket;
  wss->setParent(this);
  QObject::connect(wss, SIGNAL(sslErrors(QList<QSslError>)), wss, SLOT(ignoreSslErrors()));
  QObject::connect(wss, &QWebSocket::binaryMessageReceived, [](const QByteArray& msg) { qDebug() << msg; });
  QObject::connect(wss, &QWebSocket::textMessageReceived, [](const QString& msg) { qDebug() << msg; });
  QObject::connect(wss, qOverload<QAbstractSocket::SocketError>(&QWebSocket::error), [](QAbstractSocket::SocketError err) { qDebug() << "error" << err; });
  QObject::connect(wss, &QWebSocket::connected, []() { qDebug() << "wss connected"; });
  wss->open(_server.replace("https://", "wss://").replace("http://", "ws://") + JF_SOCKET.arg(_accessToken).arg(_deviceId));
}
