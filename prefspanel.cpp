#include "prefspanel.h"
#include "jfapi.h"
#include <QSettings>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QMessageBox>
#include <QDialogButtonBox>

#define SAVED_PASSWORD "\x01*******"

PrefsPanel::PrefsPanel(QWidget* parent) : QDialog(parent) {
  QSettings settings;
  QGridLayout* layout = new QGridLayout(this);

  QLabel* lServer = new QLabel(tr("&Server:"), this);
  fServer = new QLineEdit(this);
  lServer->setBuddy(fServer);
  if (JF_API) {
    fServer->setText(JF_API->server());
  }
  layout->addWidget(lServer, 0, 0);
  layout->addWidget(fServer, 0, 1);

  QLabel* lDeviceId = new QLabel(tr("&Device ID:"), this);
  fDeviceId = new QLineEdit(this);
  lDeviceId->setBuddy(fDeviceId);
  if (JF_API) {
    fDeviceId->setText(JF_API->deviceId());
  }
  layout->addWidget(lDeviceId, 1, 0);
  layout->addWidget(fDeviceId, 1, 1);

  QLabel* lUsername = new QLabel(tr("&Username:"), this);
  fUsername = new QLineEdit(this);
  lUsername->setBuddy(fUsername);
  fUsername->setText(settings.value("username").toString());
  layout->addWidget(lUsername, 2, 0);
  layout->addWidget(fUsername, 2, 1);

  QLabel* lPassword = new QLabel(tr("&Password:"), this);
  fPassword = new QLineEdit(this);
  fPassword->setEchoMode(QLineEdit::Password);
  if (JF_API) {
    fPassword->setText(JF_API->userId().isEmpty() ? "" : SAVED_PASSWORD);
  }
  layout->addWidget(lPassword, 3, 0);
  layout->addWidget(fPassword, 3, 1);

  bLogin = new QPushButton(tr("&Log In"), this);
  QObject::connect(bLogin, SIGNAL(clicked()), this, SLOT(logIn()));
  layout->addWidget(bLogin, 4, 1);

  buttons = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Save | QDialogButtonBox::Cancel, this);
  layout->addWidget(buttons, 5, 0, 1, 2);

  updateEnabled();
  if (JF_API) {
    QObject::connect(JF_API, SIGNAL(loggedIn()), this, SLOT(loggedIn()));
    QObject::connect(JF_API, SIGNAL(loggedOut()), this, SLOT(updateEnabled()));
    QObject::connect(JF_API, SIGNAL(loginFailed()), this, SLOT(loginFailed()));
  }
  QObject::connect(buttons->button(QDialogButtonBox::Save), SIGNAL(clicked()), this, SLOT(save()));
  QObject::connect(buttons->button(QDialogButtonBox::Ok), SIGNAL(clicked()), this, SLOT(accept()));
  QObject::connect(buttons->button(QDialogButtonBox::Cancel), SIGNAL(clicked()), this, SLOT(reject()));
  QObject::connect(fServer, SIGNAL(textChanged(QString)), this, SLOT(updateEnabled()));
  QObject::connect(fDeviceId, SIGNAL(textChanged(QString)), this, SLOT(updateEnabled()));
  QObject::connect(fUsername, SIGNAL(textChanged(QString)), this, SLOT(usernameChanged()));
  QObject::connect(fPassword, SIGNAL(textChanged(QString)), this, SLOT(updateEnabled()));
}

void PrefsPanel::updateEnabled() {
  bool hasServer = !fServer->text().isEmpty();
  bool hasUsername = !fUsername->text().isEmpty();
  bool hasPassword = !fPassword->text().isEmpty();
  bool newPassword = hasPassword && fPassword->text() != SAVED_PASSWORD;
  bool isLoggedIn = JF_API && !JF_API->userId().isEmpty();
  bLogin->setEnabled(JF_API && hasServer && hasUsername && newPassword);
  buttons->button(QDialogButtonBox::Ok)->setEnabled(isLoggedIn && hasServer && hasUsername && hasPassword && !newPassword);
}

void PrefsPanel::loggedIn() {
  setEnabled(true);
  fDeviceId->setText(JF_API->deviceId());
  fPassword->setText(SAVED_PASSWORD);
  updateEnabled();
  QMessageBox::information(this, "Justfine", tr("Successfully logged in as %1.").arg(fUsername->text()));
}

void PrefsPanel::save() {
  QSettings settings;
  if (JF_API) {
    JF_API->setServer(fServer->text());
    JF_API->setDeviceId(fDeviceId->text());
    // in case it's empty, fill in the generated ID
    fDeviceId->setText(JF_API->deviceId());
  } else {
    settings.setValue("deviceId", fDeviceId->text());
    settings.setValue("server", fServer->text());
  }
  settings.setValue("username", fUsername->text());
  if (fPassword->text().isEmpty()) {
    settings.setValue("userId", "");
  }
}

void PrefsPanel::logIn() {
  setEnabled(false);
  save();
  JF_API->login(fUsername->text(), fPassword->text());
}

void PrefsPanel::loginFailed() {
  setEnabled(true);
  QMessageBox::warning(this, "Justfine", tr("Failed to log in!"));
}

void PrefsPanel::usernameChanged() {
  QSettings settings;
  if (fPassword->text() == SAVED_PASSWORD && fUsername->text() != settings.value("username").toString()) {
    fPassword->clear();
  }
  updateEnabled();
}
