#ifndef JF_PREFSPANEL_H
#define JF_PREFSPANEL_H

#include <QDialog>

class QLineEdit;
class QPushButton;
class QDialogButtonBox;
class PrefsPanel : public QDialog {
Q_OBJECT
public:
  PrefsPanel(QWidget* parent = nullptr);

private slots:
  void updateEnabled();
  void save();
  void logIn();
  void loginFailed();
  void loggedIn();
  void usernameChanged();

private:
  QLineEdit* fServer;
  QLineEdit* fDeviceId;
  QLineEdit* fUsername;
  QLineEdit* fPassword;
  QPushButton* bLogin;
  QDialogButtonBox* buttons;
};

#endif
